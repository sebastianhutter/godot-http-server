extends Node
 
const port: int = 8080
const pfile : String = "user://persistent-file.txt"
var server: TCP_Server = TCP_Server.new()

func write_and_print_entry(text : String) -> void:
	var dt : Dictionary = OS.get_datetime(true)
	var msg : String = "{year}/{month}/{day} {hour}:{minute}:{second}: {text}".format({
		"year": dt['year'],
		"month": dt['month'],
		"day": dt['day'],
		"hour": dt['hour'],
		"minute": dt['minute'],
		"second": dt['second'],
		"text": text,
	})
	
	print(msg)

	var file: File = File.new()
	if file.file_exists(pfile):
		file.open(pfile, File.READ_WRITE)
	else:
		file.open(pfile, File.WRITE)
	
	file.seek_end()
	file.store_string("{string}\n".format({"string": msg}))
	file.close()
	
func _init() -> void:
	if (server.listen(port) != OK):
		write_and_print_entry("Failed to start http service")
	else:
		write_and_print_entry("http service started")
	

func _exit_tree() -> void:
	write_and_print_entry("http service stopped")
	server.stop()
 
func _process(_delta : float) -> void:
	if server.is_connection_available():
		var thread: Thread = Thread.new()
		thread.start(self, "_thread", server.take_connection())
		thread.wait_to_finish()
 
func _thread(con: StreamPeerTCP) -> void:
	var data: String = "hello world"
	_write_str(con, "HTTP/1.1 200")
	_write_str(con, "Content-Type: text/plain; charset=utf-8")
	_write_str(con, str("Content-Length: ", data.length()))
	_write_str(con, "")
	_write_str(con, data)
	write_and_print_entry("host {host} connected".format({"host":con.get_connected_host()}))
	con.disconnect_from_host()
 
func _write_str(con: StreamPeerTCP, string: String) -> void:
	con.put_partial_data(str(string, "\n").to_utf8())
