# docker example

This example project shows how to export a godot project as a godot container and run it.

## Requirements

You need docker and docker-compose installed.

## Usage

The docker compose file handles the build and starting of the docker container

```bash
# run docker-compose
docker-compose up
```

Afterwards you can access the http service executed by the godot project via http://localhost:8080.

## Project Settings

To make docker and godot play nicely together some configuration in the godot project is required.

### export_presets.cfg

The "docker" export preset exports a custom value `docker`. This custom value is used in the project configuration to overwrite project settings.

### project.godot Settings

A few overrides for the `docker` value are in place in project.godot

```ini
# immediately print messages to stdout, dont wait for the print buffer to fill up
# this allows us to see print() and error messages in the docker consoe
run/flush_stdout_on_print.docker=true
```

```ini
# overwrite the application name. the overwrite allows us to predefine the persistent docker volume
# for "user://" paths
config/name.docker="application"
```

```ini
# disable file logging for docker builds
# it seems the file logging cant really be disabled 
# to ensure our persistent data folder isnt flooded with logs
# we instruct godot to write any logfiles to the containers /tmp
# directory. this directory will be removed automatically when the
# container stops or restarts
file_logging/enable_file_logging.docker=false
file_logging/log_path.docker="/tmp/godot.log"
file_logging/max_log_files.docker=1
```
