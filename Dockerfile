#
# Multi Stage Docker file
# 
# Stage "headless-image": Create a docker image running the godot headless version. This image is used to export the project into a pck
# Stage "exporter": Export the given export profile
# Stage "server-image": Create a docker image running the godot server version. This image is used to run the exported pcl
# Stage "server": The last build stage, runs the exported project
#

##
# Global Build Arguments
##
ARG GODOT_VERSION=3.3.3
ARG EXPORT_NAME=docker

##
# Stage headless-image
##

FROM ubuntu:xenial as headless-image

# set godot version
# the godot version is taken from the top level ARG GODOT_VERSION
# this ensures the headless and server uses the same version
ARG GODOT_VERSION

# install godot
RUN apt-get update \
  && apt-get install -y curl unzip \ 
  && curl -LO https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
  && unzip Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
  && rm -f Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
  && mv Godot_v3.3.3-stable_linux_headless.64 /usr/bin/godot \
  && rm -rf /var/lib/apt/lists/*

# install godot export template
RUN mkdir -p "${HOME}/.local/share/godot/templates/${GODOT_VERSION}.stable" \
  && cd "${HOME}/.local/share/godot/templates/${GODOT_VERSION}.stable" \
  && curl -LO https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
  && unzip Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
  && rm -f Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
  && mv templates/* ./ \
  && rmdir templates

WORKDIR /src

##
# Stage exporter
##

FROM headless-image as exporter

# get export name from top level argument
ARG EXPORT_NAME

ADD . /src
RUN godot --path . --export "${EXPORT_NAME}" "/application.pck"

##
# Stage server-image
##

FROM ubuntu:xenial as server-image

# set godot version
# the godot version is taken from the top level ARG GODOT_VERSION
# this ensures the headless and server uses the same version
ARG GODOT_VERSION

# install godot server
RUN apt-get update \
  && apt-get install -y curl unzip \ 
  && curl -LO https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
  && unzip Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
  && rm -f Godot_v${GODOT_VERSION}-stable_linux_server.64.zip \
  && mv Godot_v3.3.3-stable_linux_server.64 /usr/bin/godot \
  && rm -rf /var/lib/apt/lists/*

# install tiny
# tiny is used to handle sigterm correctly - e.g. allows to hit ctrl+c to kill the running container
ARG TINI_VERSION=0.19.0
ADD https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini /tini
RUN chmod +x /tini

# create non root user to run godot
RUN addgroup --gid 1000 godot \ 
  && adduser --gecos "" --home /godot --disabled-password --disabled-login --uid 1000 --gid 1000 godot

# setup godot directory structure
# /godot/data is set as XDG_DATA_HOME, godot then creates its "user://" folder structure in there
# see https://docs.godotengine.org/en/3.4/tutorials/io/data_paths.html
#
# /godot/pck is used to store package files
ENV XDG_DATA_HOME=/godot/data
RUN mkdir -p /godot/data/godot/app_userdata/application /godot/pck \
  && chown -R godot:godot /godot

# export the applications data folder as volume
# this keeps the data in there persistent as a docker volume is automatically created
# for it
# the project name is overwritten in project.godot for docker builds
VOLUME /godot/data/godot/app_userdata/application

# setup workdir, default user and xdg config
WORKDIR /godot
USER godot


##
# Stage server
## 

FROM server-image as server

# add the exported pck
USER root
COPY --from=exporter "/application.pck" /godot/pck/application.pck
RUN chown godot:godot /godot/pck/application.pck
USER godot

# setup cmd command
CMD [ "/tini", "--", "/usr/bin/godot", "--main-pack", "./pck/application.pck" ]